//
//  SecondTableViewController.m
//  table2
//
//  Created by Anz joseph on 02/02/2017.
//  Copyright © 2017 Anz joseph. All rights reserved.
//

#import "SecondTableViewController.h"

@interface SecondTableViewController ()

@end

@implementation SecondTableViewController{
    NSArray *iosTeam;
    NSArray *frontEndTeam;
    NSArray *javaTeam;
    NSArray *QATeam;
    NSArray *androidTeam;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    iosTeam = [NSArray arrayWithObjects:@"gjfkh",@"jgff",@"gjjg",@"jckxls", nil];
    frontEndTeam = [NSArray arrayWithObjects:@"mghghf",@"jijghgff",@"lgjjg",@"vjckxls", nil];
    javaTeam = [NSArray arrayWithObjects:@"qqgjfkh",@"wwjgff",@"eegjjg",@"rrjckxls", nil];
    QATeam = [NSArray arrayWithObjects:@"aagjfkh",@"bbjgff",@"ccgjjg",@"ddjckxls", nil];
    androidTeam = [NSArray arrayWithObjects:@"aagjfkh",@"bbjgff",@"ccgjjg",@"ddjckxls", nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([_depname isEqualToString:@"Front End"]){
        return frontEndTeam.count;
    }
    else if ([_depname isEqualToString:@"java"]){
        return javaTeam.count;
    }
    else if ([_depname isEqualToString:@"Android"]){
        return androidTeam.count;
    }
    else if ([_depname isEqualToString:@"QA"]){
        return QATeam.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ident = @"Second";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ident forIndexPath:indexPath];
    
    if([_depname isEqualToString:@"Front End"]){
        
    }
    else if ([_depname isEqualToString:@"java"]){
        
    }
    else if ([_depname isEqualToString:@"Android"]){
        
    }
    else if ([_depname isEqualToString:@"QA"]){
        
    }
    
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
