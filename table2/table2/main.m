//
//  main.m
//  table2
//
//  Created by Anz joseph on 02/02/2017.
//  Copyright © 2017 Anz joseph. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
