//
//  AppDelegate.h
//  table2
//
//  Created by Anz joseph on 02/02/2017.
//  Copyright © 2017 Anz joseph. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

